#include "Vector.h"
#include <iostream>

//constractor and destractor
Vector::Vector(int n)
{
	n = n < 2 ? 2 : n;//if n smaller then 2
	this->_capacity = n;
	this->_size = 0;
	this->_resizeFactor = n;
	this->_elements = new int[n];
}

Vector::Vector(const Vector & other)
{
	this->_capacity = other.capacity();
	this->_size = other.size();
	this->_resizeFactor = other.resizeFactor();
	delete[] this->_elements;
	this->_elements = new int[this->_size];
	for (int i = 0; i < this->_size; i++)
	{
		*(this->_elements + i) = *(other.elements() + i);
	}
}

Vector::~Vector()
{
	delete[] this->_elements;
	this->_elements = nullptr;
}

//getters
int Vector::size() const { return this->_size; }
int Vector::capacity() const { return this->_capacity; }
int Vector::resizeFactor() const { return this->_resizeFactor; }
int* Vector::elements() const { return this->_elements; }
bool Vector::empty() const { return _size == 0 ? true : false; }

//set resize factor
void Vector::Set__resizeFactor(int n) { this->_resizeFactor = n; }

void Vector::push_back(const int & val)
{
	int* temp;
	if (_size == _capacity)
	{
		this->_capacity += this->_resizeFactor;
		temp = new int[this->_capacity];
		for (int i = 0; i <= _size; i++)
		{
			temp[i] = _elements[i];
		}
		delete[] _elements;
		_elements = temp;
	}
	_elements[_size++] = val;
}

int Vector::pop_back()
{
	int temp = 0;
	if (_size == 0)
	{
		std::cerr << "Its already empty :-)" << std::endl;
		temp = -1;
	}
	else
	{
		temp = _elements[--_size];
	}
	return temp;
}

void Vector::resize(int n)
{
	if (n > _capacity)
	{
		reserve(n);
		_capacity = n;
	}
	else
	{
		_capacity = n;
	}
}

void Vector::assign(int val)
{
	for (int i = 0; i <= _capacity; i++)
	{
		_elements[i] = val;
	}
	_size = _capacity;
}

void Vector::reserve(int n)
{
	while (this->_capacity < n)
	{
		this->_capacity += this->_resizeFactor;
		int* n_elem = new int[this->_capacity];

		for (int i = 0; i < this->_capacity; i++)
		{
			n_elem[i] = this->_elements[i];
		}

		delete[] this->_elements;
		this->_elements = n_elem;
		this->_capacity = n;
	}
}

void Vector::resize(int n, const int & val)
{
	if (n > _capacity)
	{
		reserve(n);
		for (int i = _size + 1; i <= n; i++)
		{
			_elements[i] = val;
		}
		_size = n;
	}
	else
	{
		_size = n;
	}
}

Vector & Vector::operator=(const Vector & other)
{
	if (this == &other)
	{
		return *this;
	}

	delete[] this->_elements;
	this->_size = other._size;
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_elements = new int[this->_capacity];
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = other._elements[i];
	}

	return *this;
}

int & Vector::operator[](int n) const
{
	return (n > this->_size ? _elements[0] : _elements[n]);
}
