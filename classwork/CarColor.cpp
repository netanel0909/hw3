#include "CarColor.h"

CarColor::CarColor()
{
  this->_color = Col::WHITE;
}

CarColor::CarColor(const Col& color): _color(color){}

Col CarColor::getColor() const
{
    return this -> _color;
}

bool CarColor::operator==(const CarColor &sec) const
{
    return this->_color == sec._color;
}
