#include "CarList.h"
#include <iostream>

#define ZERO 0

CarList::CarList(): _first(nullptr), _size(0){}

CarList::CarList(const CarList &other)
{
    *this = other;
}

CarList& CarList::operator=(const CarList &other)
{
    if (&other == this) 
    {
        return *this;   
    }

    this->_size = other._size;

    CarNode* otherCurr = other._first; 
    CarNode *thisCurr = new CarNode(otherCurr->getData());

    while (otherCurr->getNext() != nullptr)
    {
       
        otherCurr = otherCurr->getNext();
        thisCurr = thisCurr->getNext();  
    }

    return *this;
}

void CarList::setFirst(CarNode *first)
{ 
	this->_first = first; 
}

CarNode *CarList::getFirst() const
{
	return this->_first;
}

unsigned int CarList::getSize() const
{
	return this->_size;
}

void CarList::add(const Car carToAdd)
{
    if(this->_first == nullptr) 
    {
        this->_first = new CarNode(carToAdd);  
    }
    else
    {
        CarNode* curr = this->_first;   
        while(curr->getNext() != nullptr) 
        {
            curr = curr->getNext();
        }

        curr->setNext(new CarNode(carToAdd));   
    }
}

bool CarList::find(const Car &carToFind) const
{
	CarNode* curr = this->_first;
	while (curr != nullptr)
	{
		if (curr->getData() == carToFind)
		{
			return true;
		}
		curr = curr->getNext();
	}
}

void CarList::printMostExpansive() const
{
    if(this->_first == nullptr) 
    {
        std::cout << "Empty\n" ;
    }
    else if(this->_size == 1)
    {
        std::cout << this->_first->getData(); 
    }
    else
    {
        CarNode* curr = this->_first->getNext();
        Car mostExpansiveCar = this->_first->getData();
        while (curr != nullptr)
        {
            if (mostExpansiveCar < curr->getData());
            {
                mostExpansiveCar = curr->getData();  
            }
            curr = curr->getNext();
        }
        std::cout << mostExpansiveCar;
    }
}

bool CarList::remove(const Car carToRemove)
{
	if (this->_first == nullptr)
	{
		return false;
	}
	else if (this->_first->getData() == carToRemove)
	{
		CarNode* temp = this->_first;
		this->_first = this->_first->getNext();
		delete temp;
	}
	else
	{
		CarNode *prev = this->_first, *curr = prev->getNext();
		while (curr != nullptr)
		{
			if (curr->getData() == carToRemove)
			{
				prev->setNext(curr->getNext());
				return true;
			}
			prev = prev = prev->getNext();
			curr = prev->getNext();
		}
		return false;
	}
}

int CarList::numOfRedCars() const
{
    CarNode *curr = this->_first;
    int numOfRedCars = 0;
    while (curr != nullptr)
    {
        if (curr->getData().getCarColor().getColor() == Col::RED)
        {
            numOfRedCars++;
        }
        curr = curr->getNext();
    }
    return numOfRedCars;
}

int CarList::numOfOwnerCars(const std::string owner) const
{
  CarNode *curr = this->_first;
  int numOfOwnedCars = 0;
  while (curr != nullptr) 
  {
    if (curr->getData().getOwner() == owner) 
    {
        numOfOwnedCars++;
    }
    curr = curr->getNext();
  }
  return numOfOwnedCars;
}

void CarList::print() const
{
    CarNode *curr = this->_first;
    while (curr != nullptr)
    {
        curr->getData().print();
        std::cout << std::endl;
        curr = curr->getNext();
    }
}

void CarList::printByModule(const char c) const
{
    CarNode *curr = this->_first;
    while (curr != nullptr)
    {
        if (curr->getData().getModel()[ZERO] == c)
        {
            curr->getData().print();
        }
        curr = curr->getNext();
    }
}
