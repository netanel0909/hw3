#include "Car.h"

class CarNode
{
		Car _data;
		CarNode* _next;

    public:

        // constructor
        CarNode(const Car car);

        // getters
		Car getData() const;
        CarNode* getNext() const;
       

        // setters
		void setNext(CarNode* next);
		void setData(const Car car);
     

};