#include "CarList.h"
#include <iostream>

using std::cout;
using std::endl;


int main()
{
    Car c1("Netanel", 300, Col::RED, "ABC", "ABC");
    Car c2("Lior", 500, Col::BLUE, "DEF", "DEF");

    CarList cl;
    cl.add(c1);
    cl.add(c2);
    cl.print();

	system("PAUSE");
    return 0;
}