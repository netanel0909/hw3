#include <iostream>

enum Col
{
    RED,
    BLUE,
    GREEN,
    YELLOW,
    ORANGE,
    PURPLE,
    PINK,
    BLACK,
    WHITE,
    SILVER,
};

class CarColor
{

		Col _color;

	public:

		CarColor(const Col&);
		CarColor(); // consturctors
		Col getColor() const; // get func	
		bool operator==(const CarColor& sec) const;// operator ==
  

};