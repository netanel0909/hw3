#include "CarNode.h"

class CarList
{
		CarNode* _first;
		unsigned int _size;

    public: 

		int numOfOwnerCars(const std::string owner) const;
		CarList& operator=(const CarList& other);
		void printByModule(const char c) const;
		bool find(const Car& carToFind) const;	
		bool remove(const Car carToRemove);
		void printMostExpansive() const;       
        CarList(const CarList& other);		
		void setFirst(CarNode *first);
		unsigned int getSize() const;
		void add(const Car carToAdd);
        CarNode *getFirst() const;
		int numOfRedCars() const;
        void print() const;
		CarList();
        
};