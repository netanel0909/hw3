#include "Car.h"

using std::string;


Car::Car(string owner, double price, Col color, string model, string company): _owner(owner), _price(price), _carColor(color), _model(model), _company(company) {}

double Car::getPrice() const
{
	return this->_price;
}

CarColor Car::getCarColor() const
{
	return this->_carColor;
}

string Car::getOwner() const
{
	return this->_owner;
}

string Car::getModel() const
{
	return this->_model;
}

string Car::getCompany() const
{
	return this->_company;
}





void Car::getOwner(const string newOwner)
{
	this->_owner = newOwner;
}


void Car::getCompany(const string newCompany)
{
	this->_company = newCompany;
}

void Car::getCarColor(const CarColor newColor)
{
	this->_carColor = newColor;
}
void Car::getPrice(const double newPrice)
{
	this->_price = newPrice;
}

void Car::getModel(const string newModel)
{
	this->_model = newModel;
}




std::ostream &operator<<(std::ostream &os, const Car &car)
{
    os << "model: " << car.getModel() << std::endl << "company: " << car.getCompany() << "\n" << "price: " << car.getPrice() << "$\n" << "owner: " << car.getOwner() << "\n";
    return os;
}

void Car::print() const
{
    std::cout << *this;
}

bool Car::operator==(const Car &other) const
{   
    return (this->_price == other._price && this->_model == other._model && this->_company == other._company && this->_carColor == other._carColor);
}

bool Car::operator<(const Car &other) const
{
	return ((this->_price == other._price) ? this->_model < other._model : this->_price < other._price);
}

bool Car::operator>(const Car &other) const
{
	return (this->_price == other._price ? this->_model > other._model : this->_price > other._price);
}

bool Car::operator!=(const Car &other) const
{
    return *this != other;
}
