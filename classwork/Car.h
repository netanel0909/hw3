#include <string>
#include <iostream>
#include "CarColor.h"

class Car
{
		std::string _owner;
		double _price;
		CarColor _carColor;
		std::string _model;
		std::string _company;

	public:
    
		// Constructors
	  Car(std::string owner, double price, Col color, std::string model, std::string company);

	  // get funcs
	  std::string getOwner() const;
	  std::string getModel() const;
	  std::string getCompany() const;
	  double getPrice() const;
	  CarColor getCarColor() const;
	  
	  // set funcs
	  void getPrice(const double newPrice);
	  void getCarColor(const CarColor newColor);
	  void getOwner(const std::string newOwner);
	  void getModel(const std::string newModel);
	  void getCompany(const std::string newCompany);

	  // operators
	  bool operator==(const Car &other) const;
	  bool operator<(const Car &other) const;
	  bool operator>(const Car &other) const;
	  bool operator!=(const Car &other) const;
	  friend std::ostream &operator<<(std::ostream &os, const Car &car);
	  void print() const;//print func

};